import MockDataRecorder from 'a1xr-utilities';

class HighLowAlgoModel {

    constructor(
        seedData, 
        exchange, 
        tradeTracker, 
        logger, 
        runInterval = 60, //every minute,
        tradeLabel = ''
        ){
        
        this.seedData = seedData;
        this.exchange = exchange;
        this.tradeTracker = tradeTracker;
        this.logger = logger;
        this.runlive = true;
        this.orderIdPrefix = tradeLabel ?  "hl_" + tradeLabel : "hl";
        this.runInterval = runInterval;
        this.data = {};
        this._rawData;
    }

    async init(){
        const openOrders = {};
        const price = {};
        const bidAsk = {};
        const ohlc = {};

        var symbols = this.seedData.symbols;
        for (var symbol in symbols) {
            openOrders[symbol] = this.exchange.openOrders({
                symbol : symbol,
                timestamp : new Date().getTime()
            });

            price[symbol] = this.exchange.prices({symbol:symbol});
            bidAsk[symbol] = this.exchange.bookTicker({symbol:symbol});
            ohlc[symbol] = this.exchange.candles({symbol:symbol, interval: '1m', limit: 1});

        }
            
        this._rawData = await Promise.all([
            this.exchange.exchangeInfo(), 
            this.exchange.accountInfo(),
            openOrders,
            price,
            bidAsk,
            ohlc
        ]);
    }

    async load(){
        var modeledData = {};
        var rawData = this._rawData;
        var baseCurrency = this.seedData.tradeDetails.baseCurrency;
        var baseFunds = rawData[1].balances.find(item =>{
            return item.asset == baseCurrency;
        });

        var symbols = this.seedData.symbols
        for (var symbol in symbols) {
            var openOrders = await rawData[2][symbol];
            var quote = {
                price : await rawData[3][symbol],
                bidask : await rawData[4][symbol],
                ohlc : await rawData[5][symbol],
            };

            modeledData[symbol] = {
                seedData : symbols[symbol],
                exchangeInfo : rawData[0].symbols.find(item=>{
                    return item.symbol == symbol;
                }),
                accountInfo : rawData[1].balances.find(item=>{
                    return symbol.indexOf(item.asset) >= 0 && item.asset != baseCurrency && item.asset != "TUSD";
                }),
                openOrders : openOrders,
                quote : quote,
                baseFunds : baseFunds
            };

            modeledData[symbol].exchangeInfo.serverTime = rawData[0].serverTime;
        }
        
        this.data = modeledData;

        if(MockDataRecorder.recordData){
            MockDataRecorder.writeFile('hi-low-models', 'hi-low-models.json', this._rawData);
        }
    }
}

module.exports = HighLowAlgoModel;
