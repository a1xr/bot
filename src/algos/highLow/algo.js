

import {Decimal} from 'decimal.js';
import BinanceOrderHelpers from 'a1xr-binance-exchange-proxy';
import UTCDate from 'a1xr-utilities';

/*
*   Basic algorithm to sell high and buy low. Intent is for assets more like BTC that have a stable consistant value or upside.
*   The idea is that BTC has a 4-6% flux during the day.
*/
class HighLowAlgo {
    constructor(model){
        this.model = model;
        //exchange, logger, orderIdPrefix, runlive
        this.orderHelper = new BinanceOrderHelpers({
            exchange : model.exchange,
            logger: model.logger,
            tradeTracker : model.tradeTracker,
            orderIdPrefix : model.orderIdPrefix,
            runlive : model.runlive
        });

        this._lastPriceSet = UTCDate.getUTCDate(-1); //triggers set in step function
        this._stopLossPrice = 0;
    }

    stopLossSettingTracker(price, stopLossPct){
        var curDate = UTCDate.getUTCDate();
        if(this._lastPriceSet.getTime() < curDate.getTime()){
            var price = new Decimal(price)
            var stopLossMod = price.times(new Decimal(stopLossPct));
            this._stopLossPrice = price.minus(stopLossMod);

            this._lastPriceSet = UTCDate.getUTCDate(6);
        }
    }

    triggerStopLoss(price){
        return new Decimal(price).lessThan(this._stopLossPrice);
    }

    step(){
        var data = this.model.data;
        for(var symbol in data){
            var assetModel = data[symbol];

            /*
                args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee, priceModifierPct
            */
            var minQty = new Decimal(this.model.seedData.tradeDetails.lotSize.minQty);
            var freeQty = new Decimal(assetModel.accountInfo.free);

            var orderArgs = {
                symbol : symbol,
                qtyPrecision : assetModel.seedData.qtyPrecision,
                pricePrecision : assetModel.seedData.pricePrecision,
                currentPrice : assetModel.quote.price.price,
                stopLossPct : this.model.seedData.tradeDetails.stopLossPct,
                tradeFee : this.model.seedData.tradeDetails.tradeFee,
                priceModifierPct : this.model.seedData.priceModifierPct
            };

            this.stopLossSettingTracker(orderArgs.currentPrice, orderArgs.stopLossPct);
            var hasOpenOrder = this.orderHelper.hasOpenOrder(assetModel.openOrders);

            var staleBuyOrder = this.orderHelper.hasStaleBuyOrder(assetModel.openOrders, assetModel.exchangeInfo.serverTime, 30);

            if(staleBuyOrder){
                console.log('staleorder')
                this.orderHelper.cancelOrder(staleBuyOrder);

            }else if(this.triggerStopLoss(orderArgs.currentPrice)){
                this.orderHelper.placeImmediateSellOrder(orderArgs);

            //BUY
            }else if(
                !hasOpenOrder  
                && freeQty.lessThanOrEqualTo(minQty*2)
            ){
                orderArgs.orderValue = assetModel.seedData.orderValue;

                this.orderHelper.placeBuyOrder(orderArgs);

            //SELL
            }else if(
                !hasOpenOrder
                && freeQty.greaterThan(minQty)
            ){

                orderArgs.qty = Math.min(
                    freeQty.minus(minQty).toFixed(assetModel.seedData.qtyPrecision), 
                    this.model.seedData.tradeDetails.lotSize.maxQty
                );
                
                this.orderHelper.placeSellOrder(orderArgs);
            }
        }
    }

    _run(){
        this.model.init()
        .then(()=>{
            this.model.load()
            .then(()=>{
                this.step();
            });
        });
    }

    run() {
        this._run();
        setInterval(args=>{
            this._run();
        }, this.model.runInterval * 1000)
    }
}

module.exports = HighLowAlgo;
