import BinanceIndicatorFactory from 'a1xr-binance-exchange-proxy';

class LinearRegressionAlgoModel {

    constructor(
        seedData, 
        exchange, 
        tradeTracker, 
        logger, 
        limitFundsNum = 3, 
        runInterval = 5
        ){
        
        this.seedData = seedData;
        this.exchange = exchange;
        this.tradeTracker = tradeTracker;
        this.logger = logger;
        this.limitFundsNum = limitFundsNum;
        this.runlive = true;
        this.orderIdPrefix = "lrc";
        this.runInterval = runInterval;
        this.data = {};
        this._rawData;
        this.indicatorFactory = new BinanceIndicatorFactory(this.exchange);
    }

    async init(){
        const openOrders = {};
        const lrcIndicators = {};
        const shortMaIndicators = {};
        const longMaIndicators = {};
        const price = {};
        const bidAsk = {};
        const ohlc = {};

        var symbols = this.seedData.symbols;
        for (var symbol in symbols) {
            openOrders[symbol] = this.exchange.openOrders({
                symbol : symbol,
                timestamp : new Date().getTime()
            });

            lrcIndicators[symbol] = this.indicatorFactory.getLinearRegressionChannel(symbol, '5m', 50);
            shortMaIndicators[symbol] = this.indicatorFactory.getMovingAverage(symbol, '5m', 5, 7);
            longMaIndicators[symbol] = this.indicatorFactory.getMovingAverage(symbol, '5m', 25, 7);
            price[symbol] = this.exchange.prices({symbol:symbol});
            bidAsk[symbol] = this.exchange.bookTicker({symbol:symbol});
            ohlc[symbol] = this.exchange.candles({symbol:symbol, interval: '1m', limit: 1});

        }
            
        this._rawData = await Promise.all([
            this.exchange.exchangeInfo(), 
            this.exchange.accountInfo(),
            openOrders,
            lrcIndicators,
            shortMaIndicators,
            longMaIndicators,
            price,
            bidAsk,
            ohlc
        ]);
    }

    async load(){
        var modeledData = {};
        var rawData = this._rawData;
        var baseCurrency = this.seedData.tradeDetails.baseCurrency;
        var baseFunds = rawData[1].balances.find(item =>{
            return item.asset == baseCurrency;
        });

        var symbols = this.seedData.symbols
        for (var symbol in symbols) {
            var openOrders = await rawData[2][symbol];
            var lrcIndicator = await rawData[3][symbol]();
            var shortMaIndicator = await rawData[4][symbol]();
            var longMaIndicator = await rawData[5][symbol]();
            var quote = {
                price : await rawData[6][symbol],
                bidask : await rawData[7][symbol],
                ohlc : await rawData[8][symbol],
            };

            modeledData[symbol] = {
                seedData : symbols[symbol],
                exchangeInfo : rawData[0].symbols.find(item=>{
                    return item.symbol == symbol;
                }),
                accountInfo : rawData[1].balances.find(item=>{
                    return symbol.indexOf(item.asset) >= 0 && item.asset != baseCurrency && item.asset != "TUSD";
                }),
                openOrders : openOrders,
                lrcIndicator : lrcIndicator,
                shortMaIndicator : shortMaIndicator,
                longMaIndicator : longMaIndicator,
                quote : quote,
                baseFunds : baseFunds
            };
        }
        this.data = modeledData;
    }
}

module.exports = LinearRegressionAlgoModel;
