
//Based on the slope of a line make daily trade decisions on what to buy into.
//Possible use line of best fit
//https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
//
// Algo calculate line of best fit
// if slope is up buy x at x
    //Once buy occurs calculate sell
    //assume we have stopLimit on order through seperate algo

// if slope is down or flat do not trade
//if slope is flat consider sell.
import {Decimal} from 'decimal.js';
import BinanceOrderHelpers from 'a1xr-binance-exchange-proxy';


class LinearRegressionAlgo {
    constructor(model){
        this.model = model;
        //exchange, logger, orderIdPrefix, runlive
        this.orderHelper = new BinanceOrderHelpers({
            exchange : model.exchange,
            logger: model.logger,
            orderIdPrefix : model.orderIdPrefix,
            runlive : model.runlive
        });
    }

    step(){
        var data = this.model.data;
        for(var symbol in data){
            var assetModel = data[symbol];
            var indicator = assetModel.lrcIndicator;
            var quote = assetModel.quote;
            var ohlc = quote.ohlc && quote.ohlc.length ? quote.ohlc[0] : {};

            if(indicator.getChannelSlope() > 0){
                var closeTime = indicator.matchPrecision(ohlc.closeTime);

                /*
                args: symbol, orderValue, qty, qtyPrecision, currentPrice, stopLossPct, tradeFee
                */
                var orderArgs = {
                    symbol : symbol,
                    qtyPrecision : assetModel.seedData.qtyPrecision,
                    currentPrice : assetModel.quote.price.price,
                    stopLossPct : this.model.seedData.tradeDetails.stopLossPct,
                    tradeFee : this.model.seedData.tradeDetails.tradeFee
                };

                //BUY
                if(
                    indicator.isBelowChannel(closeTime, ohlc.low) 
                    && indicator.getNumTouchesBottom() > 1
                    && assetModel.openOrders.length == 0    
                ){

                    orderArgs.orderValue = assetModel.seedData.orderValue;

                    this.orderActions.placeBuyOrder(orderArgs);

                //SELL
                }else if(
                    indicator.isAboveChannel(closeTime, ohlc.high)
                    && new Decimal(assetModel.accountInfo.free).greaterThan(new Decimal(this.model.seedData.tradeDetails.lotSize.minQty))
                ){

                    orderArgs.qty = Math.min(
                        Number(assetModel.accountInfo.free), 
                        this.model.seedData.tradeDetails.lotSize.maxQty
                    );

                    this.orderActions.placeSellOrder(orderArgs);
                }
            }
        }
    }

    _run(){
        this.model.init()
        .then(()=>{
            this.model.load()
            .then(()=>{
                this.step();
            });
        });
    }

    run() {
        this._run();
        setInterval(args=>{
            this._run();
        }, this.model.runInterval * 1000)
    }
}

module.exports = LinearRegressionAlgo;
