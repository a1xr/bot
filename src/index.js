import minimist from 'minimist';
import "@babel/polyfill";
import BinanceFactory from 'a1xr-binance-exchange-proxy';
import 'dotenv/config';
import MySqlStore from 'a1xr-utilities';
import TradeTrackerModel from './models/tradeTrackerModel';
import TradeLoggerModel from './models/tradeLoggerModel';

import LinerRegressionAlgo from './algos/linearRegression/algo';
import LinearRegressionAlgoModel from './algos/linearRegression/model';
import HighLowAlgo from './algos/highLow/algo';
import HighLowAlgoModel from './algos/highLow/model';

var config = require('./config.json')[global.config || 'highlowStandard'];

var path = require('path');
global.appRoot = path.resolve(__dirname);

const algoMap = {
  'linear-regression' : {
    algo : LinerRegressionAlgo,
    model : LinearRegressionAlgoModel
  },
  'high-low' : {
    algo : HighLowAlgo,
    model : HighLowAlgoModel
  }
};

//commandline args
let args = minimist(process.argv.slice(2), {  
  alias: {
      t : 'test', //run the thing live
  },
  default : {
    t : 0,
  }
});

//setup db connection
let db = new MySqlStore({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USERNAME,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DBNAME
});

//main data model
let tradeTracker = new TradeTrackerModel(db);
let logger = new TradeLoggerModel(db);

//exchange connection
let exchange = BinanceFactory(process.env.BINANCE_API_KEY, process.env.BINANCE_SECRET);

  if(!algoMap[config.algorithm]){
    console.log("Algo not found");
    process.exit(1);
  }

  var algoModule = algoMap[config.algorithm];
  const seedData = require(config.seedFile);

  var model = new algoModule.model(seedData, exchange, tradeTracker, logger, config.frequency, config.orderLabel);
  model.runlive = args.t ? 0 : config.live;
  
  var algo = new algoModule.algo(model);
  
  algo.run(); //kickit


module.exports = function () {};



