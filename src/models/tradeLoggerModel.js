

class TradeLoggerModel {
    constructor( database ) {
        this.database = database;
        this.tableName = "trade_log";
        this.constants = {
            ERROR : "ERROR",
            SUCCESS : "SUCCESS"
        };

        let createTable = `create table if not exists `+this.tableName+`(
            id INT AUTO_INCREMENT PRIMARY KEY,
            quote MEDIUMTEXT not null,
            trade_response MEDIUMTEXT not null,
            note varchar(255) not null,
            entry_type varchar(255) not null,
            ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
          )`;
          
          database.query(createTable, (err, result) => {
            if (err) throw err;
            //console.log(result);
          });
    }

    log(quote, tradeResponse, note, entryType = "SUCCESS"){
        let sql = 'INSERT INTO ' +this.tableName+ ' SET ?';
        return this.database.query(sql, 
            {
                quote : JSON.stringify(quote), 
                trade_response : JSON.stringify(tradeResponse), 
                note : note,
                entry_type : entryType
            }
            );
    };
}

module.exports = TradeLoggerModel
